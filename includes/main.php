<main class="container">
    <div class="row">
            <div class="col-sm-2 col-xs-2"></div>
            <h2 class="col-sm-8 col-xs-8" id="titulo-main">Nosotros hacemos las mejores pastas</h2>
            <div class="col-sm-2 col-xs-2"></div>
    </div>
    <div class="row">
        <div class="col-sm-2 col-xs-2"></div>
        <h3 class="col-sm-8 col-xs-8" id="subtitulo-main">Vos las haces unicas</h3>
        <div class="col-sm-2 col-xs-2"></div>
    </div>
    <hr>
    <div class="row">
        <div class="col-sm-2 col-xs-2"></div>
        <p class="col-sm-8 col-xs-2" id="parrafo-main">Trabajamos todos los días para ofrecerte lo mejor. Pero son tus manos, tu talento y amor los que transforman nuestros productos en una experiencia incomparable.</p>
        <div class="col-sm-2 col-xs-2"></div>
    </div>
    
    <!--Seccion Que hacemos-->

    <section id="seccion-menu">
        <div class="row">
            <div class="col-md-12" id="historia">
                <h3 id="titulo-historia">-Historia-</h3>
            </div>        
        </div>
        <div class="row">
            <div class="col-md-4 bottom-menu" id="pastas">
                <h3 id="titulo-pastas">-Pastas-</h3>
            </div>
            <div class="col-md-4 bottom-menu" id="pastas-rellenas">
                <h3 id="titulo-pastas-rellenas">-Pastas Rellenas-</h3>
            </div>
            <div class="col-md-4 bottom-menu" id="tucos-y-salsas">
                <h3 id="titulo-tucos-y-salsas">-Tucos y Salsas</h3>
            </div>
        </div>
    </section>
    <hr>
    <section class="container" id="seccion-agradecimientos">
        <h2 id="titulo-agradecimientos">Agradecimientos</h2>
        <h3 id="subtitulo-agradecimientos">larala</h3>
        <hr>
        <div class="container">
            <p id="parrafo-agradecimientos">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perferendis, sunt alias? Quo autem perspiciatis quod placeat cupiditate, nulla et accusantium doloremque, dolorum quos dolores dicta, odio ullam deleniti voluptatem nisi?</p>
        </div>
        <div class="row">
            <div class="imagenes-agradecimientos" id="imagen-agradecimiento-1"></div>
            <div class="row">
                <h3>titulo</h3>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Magnam rem quo iste expedita atque sit temporibus, odio, incidunt ipsum voluptatum quis ullam et provident! Eveniet corporis delectus harum odio quod!</p>
            </div>
        </div>
    </section>

</main>
