let btnHistoria = document.querySelector("#historia");
let btnPastas = document.querySelector("#pastas");
let btnPastasRellenas = document.querySelector("#pastas-rellenas");
let btnTucos = document.querySelector("#tucos-y-salsas");
let agradecimientos = document.querySelector("#seccion-agradecimientos");


//Titulos

let tituloHistoria = document.querySelector("#titulo-historia")
let tituloPastas   = document.querySelector("#titulo-pastas")
let titulospastasRe= document.querySelector("#titulo-pastas-rellenas")
let tituloTuco     = document.querySelector("#titulo-tucos-y-salsas")

//======Eventos======


//===Evento btnHistoria===
btnHistoria.addEventListener("click", ()=>{
    if(document.querySelector("#parrafoHistoria") == null ){
        let parrafo = document.createElement("p");
        let titulo = document.querySelector("#titulo-historia")
        titulo.style.marginTop="0.5em"
        titulo.classList.add('animated', 'bounceInUp')
        parrafo.id = "parrafoHistoria"
        parrafo.classList.add('parrafoMenu', 'animated', 'bounceInUp')
        parrafo.innerText="Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro ipsa nihil sequi iure. Incidunt possimus laborum quia commodi exercitationem aliquid omnis, quibusdam porro nulla sed dolores voluptates sunt? Tenetur, et.";
        parrafo.style.fontSize="2em"
        btnHistoria.appendChild(parrafo);
        tituloHistoria.style.margintop="2em";
    }
    else{
        let parrafo = document.querySelector("#parrafoHistoria")
        let titulo = document.querySelector("#titulo-historia")
        titulo.style.marginTop="3em"
        parrafo.classList.remove("bounceInUp")
        parrafo.classList.add("bounceOutDown")
        titulo.classList.remove("bounceInUp")
        titulo.classList.add("bounceInDown")
        btnHistoria.removeChild(parrafo);
    }
})


//===Evento btnPastas===
btnPastas.addEventListener("click", ()=>{
    if(document.querySelector("#parrafoPastas") == null ){
        let parrafo = document.createElement("p");
        let titulo  = document.querySelector("#titulo-pastas")
        titulo.style.marginTop="0.5em"
        titulo.classList.add('animated', 'bounceInUp')
        parrafo.id = "parrafoPastas"
        parrafo.classList.add('parrafoMenu', 'animated', 'bounceInUp')
        parrafo.innerText="Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro ipsa nihil sequi iure. Incidunt possimus laborum quia commodi exercitationem aliquid omnis, quibusdam porro nulla sed dolores voluptates sunt? Tenetur, et.";
        btnPastas.appendChild(parrafo);
        btnPastas.style.backgroundColor="rgba(0,0,0,0.8);"
    }
    else{
        let parrafo = document.querySelector("#parrafoPastas")
        let titulo = document.querySelector("#titulo-pastas")
        parrafo.classList.remove('bounceInUp')
        parrafo.classList.add('bounceOutDown')
        titulo.classList.remove('bounceInUp')
        titulo.classList.add('bounceInDown')
        titulo.style.marginTop="3.5em"
        btnPastas.removeChild(parrafo);
    }
})

//===Evento btnPastasRellenas===
btnPastasRellenas.addEventListener("click", ()=>{
    if(document.querySelector("#parrafoPastasRellenas") == null ){
        let parrafo = document.createElement("p");
        let titulo  = document.querySelector("#titulo-pastas-rellenas")
        titulo.style.marginTop="0.5em"
        titulo.classList.add('animated', 'bounceInUp')
        parrafo.id = "parrafoPastasRellenas"
        parrafo.classList.add('parrafoMenu', 'animated', 'bounceInUp')
        parrafo.innerText="Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro ipsa nihil sequi iure. Incidunt possimus laborum quia commodi exercitationem aliquid omnis, quibusdam porro nulla sed dolores voluptates sunt? Tenetur, et.";
        btnPastasRellenas.appendChild(parrafo);
    }
    else{
        let parrafo = document.querySelector("#parrafoPastasRellenas")
        parrafo.classList.remove("bounceInUp")
        parrafo.classList.add("bounceOutDown")
        let titulo = document.querySelector("#titulo-pastas-rellenas")
        titulo.classList.remove("bounceInUp")
        titulo.classList.add("bounceInDown")
        titulo.style.marginTop="3.5em"
        btnPastasRellenas.removeChild(parrafo)
    }
})

//===Evento btnTucos===
btnTucos.addEventListener("click", ()=>{
    if(document.querySelector("#parrafoTucos") == null ){
        let parrafo = document.createElement("p");
        let titulo = document.querySelector("#titulo-tucos-y-salsas")
        titulo.style.marginTop="0.5em";
        titulo.classList.add('animated', 'bounceInUp')
        parrafo.id = "parrafoTucos"
        parrafo.classList.add("parrafoMenu", 'animated', 'bounceInUp')
        parrafo.innerText="Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro ipsa nihil sequi iure. Incidunt possimus laborum quia commodi exercitationem aliquid omnis, quibusdam porro nulla sed dolores voluptates sunt? Tenetur, et.";
        btnTucos.appendChild(parrafo);

    }
    else{
       
        let parrafo = document.querySelector("#parrafoTucos")
        // btnTucos.removeChild(parrafo);
        parrafo.classList.remove('bounceInUp')
        parrafo.classList.add('bounceOutDown')
        let titulo = document.querySelector("#titulo-tucos-y-salsas")
        titulo.classList.remove("bounceInUp")
        titulo.classList.add("bounceInDown")
        titulo.style.marginTop="3.5em"
        btnTucos.removeChild(parrafo)
      
    }

})

