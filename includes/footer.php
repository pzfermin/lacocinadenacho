<footer>
              <!-- Footer Elements -->
    <div class="container container-footer">

        <!-- Grid row-->
        <div class="row">
  
            <!-- Grid column -->
            <div class="col-md-12 py-5">
                <div class="mb-5 flex-center">
    
                <!-- Facebook -->
                <a href="https://www.facebook.com/La-Cocina-De-Nacho-454230878264656/?epa=SEARCH_BOX" class="fb-ic">
                    <i class="fa fa-facebook fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </a>
                <!-- Twitter -->
                <a href="#"class="tw-ic">
                    <i class="fa fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </a>
                <!-- Google +-->
                <a href="#" class="gplus-ic">
                    <i class="fa fa-google-plus fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </a>
                <!--Linkedin -->
                <a href="#"class="li-ic">
                    <i class="fa fa-linkedin fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </a>
                <!--Instagram-->
                <a href="https://www.instagram.com/lacocinadenachopastas/"class="ins-ic">
                    <i class="fa fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                </a>
                <!--Pinterest-->
                <a href="https://ar.pinterest.com/pzfermin/" class="pin-ic">
                    <i class="fa fa-pinterest fa-lg white-text fa-2x"> </i>
                </a>
                </div>
            </div>
          <!-- Grid column -->
  
        </div>
        <!-- Grid row-->
  
      </div>
      <!-- Footer Elements -->
  
      <!-- Copyright -->
      <div class=" text-center py-3" id="Copyright">© 2019 Copyright:
        <a href="http://www.lacocinadenacho.com" id="go-home"> LaCocinaDeNacho.com</a>
      </div>
      <!-- Copyright -->
  
    </footer>
    <!-- Footer -->



<script src="includes/scriptMenuMain.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>