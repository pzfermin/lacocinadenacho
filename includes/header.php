<header>
    <!--Nav-->
    <div class="nav-container">
        <nav class="navbar navbar-expand-lg  navbar-dark headroom animated fixed-top">
            <img src="images/Logotipo/LogoLCDNB.png" alt="logoLaCocinadenacho" id="logoLaCocinaDeNacho">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php#home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Mis Servicios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Sobre mi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contactame</a>
                    </li>
                </ul>
            </div>
        </nav>  
     </div>
    <!--Slider-->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="images/imagenes/dish-food-seafood-recipe-cuisine-pasta.jpg" alt="dish-food-seafood-recipe-cuisine-pasta">
            <div class="carousel-caption d-sm-block">
                <h3 class="titulos-slider animated  hinge delay-4s slow">¿Estas preparado...</h3>
            </div>    
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/imagenes/food-baking-pasta-icing-eggs-flour.jpg" alt="food-baking-pasta-icing-eggs-flour">
            <div class="carousel-caption d-sm-block">
                <h3 class="titulos-slider animated  hinge delay-4s slow">para...</h3>
            </div>    
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/imagenes/leaf-flower-glass-food-salad-produce.jpg" alt="leaf-flower-glass-food-salad-produce">
            <div class="carousel-caption d-sm-block">
                <h3 class="titulos-slider animated  hinge delay-4s slow">probar...</h3>
            </div>    
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/imagenes/plant-fruit-flower-dish-food-red.jpg" alt="plant-fruit-flower-dish-food-red">
            <div class="carousel-caption d-sm-block">
                <h3 class="titulos-slider animated  hinge delay-4s slow">las mejores...</h3>
            </div>    
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/imagenes/spaghetti-bolognese-food-rustic-mince-meat.jpg" alt="spaghetti-bolognese-food-rustic-mince-meat">
            <div class="carousel-caption d-sm-block">
                <h3 class="titulos-slider animated  hinge delay-4s slow">pastas de...</h3>
            </div>    
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/imagenes/plant-dish-meal-food-garlic-produce.jpg" alt="plant-dish-meal-food-garlic-produce">
            <div class="carousel-caption d-sm-block">
                <h3 class="titulos-slider animated  hinge delay-4s slow">Monte Hermoso?</h3>
            </div>
        </div>
    </div>
    </div>

<header>